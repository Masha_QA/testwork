package org.itstep.classwork.tasks;
import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println ("Введите длину ребра куба ");
        int a = scanner.nextInt();
        System.out.println("Длина ребра куба " +a);
        double result1=Math.pow(a, 3);
        System.out.println ("Объем куба " + result1);
        double result2 =4*(a*a);
        System.out.println ("Площадь боковой поверхности " + result2);
    }
}
