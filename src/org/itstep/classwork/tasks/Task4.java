package org.itstep.classwork.tasks;

public class Task4 {
    public static void main(String[] args) {
        int a = 3600;
        int day = 0;
        int h = 0;
        int min = 0;
        int sec = 0;
        if (a >= 86400) {
            day = a / 86400;
            int ost1 = a - (day * 86400);
            h = ost1 / 3600;
            int ost2 = a - ((day * 86400) + (h * 3600));
            min = ost2 / 60;
            int ost3 = a - ((day * 86400) + (h * 3600) + (min * 60));
            sec = ost3;
            {
                System.out.println(day + " дн " + h + " час " + min + " мин " + sec + " сек");
            }
        } else if (a < 86400 && a >= 3600) {
            h = a / 3600;
            int ost4 = a - (h * 3600);
            min = ost4 / 60;
            int ost5 = a - ((h * 3600) + (min * 60));
            sec = ost5;
            {
                System.out.println(h + " час " + min + " мин " + sec + " сек");
            }
        } else if (a < 3600 && a >=60) {
            min = a / 60;
            int ost6 = a - (min * 60);
            sec = ost6;
            {
                System.out.println(min + " мин " + sec + " сек");
            }
        } else {
            System.out.println(a + " сек");
        }
    }
}



